# EclipseCon Europe 2018

We are proud to support the EclipseCon Europe 2018 in Ludwigsburg with the following contributions.

![EclipseCon 2018 Logo](https://www.eclipsecon.org/sites/default/files/ECE%20Logo%202018_300x250px%20square_0.png)

## JUG Partner

We offered our service as a [JUG Partner](https://www.eclipsecon.org/europe2018/jugs) to the conference.

## Community Day

We look forward to present our specification work in the "Eclipse Project for JAX-RS" and our code contributions to "Eclipse Jersey" on the [Community Day](https://www.eclipsecon.org/europe2018/eclipse-community-day) (unconference).

## Program Sessions

We are proud to talk with *[Kevin Sutter](https://www.eclipsecon.org/europe2018/sessions/jakarta-ee-community-jax-rs-team)* about being among the first active specification projects of Jakarta EE.
